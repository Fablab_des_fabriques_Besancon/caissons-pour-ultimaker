# Capot pour système de filtration des Ultimaker 2+ et 3 

Ce capot est destiné à sécuriser nos impressions 3D par un système de filtration d'air Alveo3D afin de réduire odeurs, composés organiques volatiles (COVs) et nanoparticules émises durant l'impression.
Nous avons opté pour la solution intégrée dans un caisson permettant de réduire le bruit et maintenir une température assez haute facilitant l'impression des matériaux sujets au décollement.
Le kit fourni comporte un boitier de ventilation équipé d'un filtre ainsi qu'une carte de contrôle qui sera fixé en façade du caisson.

Le caisson est conçu pour être découpe dans du plexiglas de 3mm d'épaisseur, ainsi que la porte.
Certains éléments sont à imprimer en PLA (ou PLA renforcé) : angles, charnières, pignée de porte.

L'axe des charnières a été tourné en laiton mais vous pouvez utiliser une tige de diamètre 6mm et 20mm de long

Le maintient du caisson est assuré par des cornières d'angle en aluminium de 30x30mm, pour l'assemblage nous utiliserons des vis M4x10 ou M4x20 permettant de les utiliser comme passe-cables.
Les charnières et la poignée de porte sont collées à la porte. Celle-ci est maintenue contre l'imprimante par un aimant de diamètre 8mm et d'épaisseur 3mm logé derrière la poignée et qui se magnétise à axe présent dans l'imprimante. Le positionnement est donc plus délicat : nous vous conseillons de positionner la porte et son aimant et de la maintenir en place avant de coller les charnières dessus.



![alt text](images/capot_um2+.jpg)
